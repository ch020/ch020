#include<stdio.h>
void swap(int *x,int *y)
{
    int t;
    t = *x;
    *x = *y;
    *y = t;
}
int main()
{
    int num1,num2;
   printf("Enter value of number1: ");
    scanf("%d",&num1);
    printf("Enter value of number2: ");
    scanf("%d",&num2);

    printf("Before Swapping:\n number1 is: %d \n number2 is: %d",num1,num2);

    swap(&num1,&num2);

    printf("\nAfter Swapping:\n number1 is: %d \n number2 is:%d",num1,num2);

   return 0;
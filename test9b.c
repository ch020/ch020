#include<stdio.h>
int main () 
{ 
    int first, second, *p, *q, sum, difference, multiply, divide, reminder;  
    printf ("Enter any two integers to perform arithmetic operation:\n");  
    scanf ("%d%d", &first, &second);
    p = &first;
   q = &second;
    sum = *p + *q;
    difference = *p - *q;
    multiply = *p * *q;
    divide = *p / *q;
    reminder = *p % *q;
    printf ("Sum of the entered numbers = %d\n", sum);
    printf ("Difference of the entered numbers = %d\n", difference);
    printf ("Product of the entered numbers = %d\n", multiply);
    printf ("Quotient of the entered numbers = %d\n", divide);
    printf ("Reminder of the entered numbers = %d\n", reminder);
    return 0;
}